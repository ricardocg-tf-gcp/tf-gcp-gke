# GKE Cluster Module - Crabi.

- This module deploys a GCP hosted Kubernetes cluster using GKE service, Following the next requirements: 

1. The cluster MUST live and use custom VPC and subnetworks.
2. Cluster's subnetworks MUST NOT be calculated by GKE service, but rather provided as
custom values or calculated using any other method.
3. The cluster MUST NOT use the default node pool, but rather have an independent node
pool provisioned alongside the cluster.
4. Communication with the cluster MUST be done using a public endpoint. Private endpoint
MUST NOT be enabled.
5. The node pool MUST NOT have public IPs enabled by default.
6. Cluster's public endpoint must be accessible through the public internet.
7. For security purposes, there MUST be at least one security layer configured to ensure
only allowed users can authenticate to the cluster.
8. Kubernetes default dashboard MUST NOT be enabled by default.

# Inputs 

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| VPC | The ID of the VPC | String | - |:yes:|

# Outputs 

| Name | Description |
|------|-------------|
| name | The name of the cluster master. |
| endpoint | The IP address of the cluster master. |
| endpoint | The IP address of the cluster master. |
| endpoint | The IP address of the cluster master. |
| endpoint | The IP address of the cluster master. |

# Usage

  
